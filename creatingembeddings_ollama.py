#'ollama serve' server must be started, model used in this script must be pulled into ollama server before running this script

"""
This script creates a database of information gathered from local text files.
"""

from langchain_community.document_loaders import DirectoryLoader, TextLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
#from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.embeddings import OllamaEmbeddings
from langchain_community.vectorstores import FAISS

# define what documents to load
loader = DirectoryLoader("./", glob="*.txt", loader_cls=TextLoader)

# interpret information in the documents
documents = loader.load()
splitter = RecursiveCharacterTextSplitter(chunk_size=512,
                                          chunk_overlap=40)
texts = splitter.split_documents(documents)
#embeddings = HuggingFaceEmbeddings(
#    model_name="sentence-transformers/all-MiniLM-L6-v2",
#    model_kwargs={'device': 'cpu'})
embeddings = OllamaEmbeddings(base_url="http://localhost:11434", model="mistral")
#embeddings = OllamaEmbeddings(base_url="http://localhost:11434", model="mistral:7b-instruct-v0.2-q8_0")

# create and save the local database
db = FAISS.from_documents(texts, embeddings)
db.save_local("faiss")

