#!/usr/bin/python3

#'ollama serve' server must be started, model used in this script must be pulled into ollama server before running this script

#from langchain.embeddings import HuggingFaceEmbeddings
from langchain_community.embeddings import OllamaEmbeddings
from langchain_community.vectorstores import FAISS
from langchain.prompts import PromptTemplate
from langchain.chains import RetrievalQA
#from langchain.llms import Ollama
#from langchain.chat_models import ollama
from langchain_community.llms import Ollama
import time
import os
import streamlit as st
from streamlit_chat import message

# prepare the template we will use when prompting the AI
#template = """Use the following pieces of information to answer the user's question.
template = """The following piece of information will be refered to as 'mydoc', and you will use it to answer the user's question.
If you don't know the answer, just say that you don't know, don't try to make up an answer.
Context: {context}
Question: {question}
"""
#Only return the helpful answer below and nothing else.
#Helpful answer:

# load the language model
#llm = CTransformers(model='./llama-2-7b-chat.ggmlv3.q4_0.bin',
#                    model_type='llama',
#                    config={'max_new_tokens': 256, 'temperature': 0.01})
#llm = Ollama(model="mistral:7b-instruct-v0.2-q8_0")
llm = Ollama(model="customMistral")

# load the interpreted information from the local database
#embeddings = HuggingFaceEmbeddings(
#    model_name="sentence-transformers/all-MiniLM-L6-v2",
#    model_kwargs={'device': 'cpu'})
embeddings = OllamaEmbeddings(base_url="http://localhost:11434", model="mistral:7b-instruct-v0.2-q8_0")
#db is our vector store
db = FAISS.load_local("faiss", embeddings)

# prepare a version of the llm pre-loaded with the local content
#retriever = db.as_retriever(search_type="similarity", search_kwargs={'k': 2})
retriever = db.as_retriever(search_type="mmr", search_kwargs={'k': 2})
prompt = PromptTemplate(
    template=template,
    input_variables=['context', 'question'])
qa_llm = RetrievalQA.from_chain_type(llm=llm,
                                     chain_type='stuff',
                                     retriever=retriever,
                                     return_source_documents=True,
                                     chain_type_kwargs={'prompt': prompt})

st.title("ollama chat")

# Function for conversational chat
def conversational_chat(query):
    res = qa_llm({'query':query})
    #res = blabla({"question": query, "chat_history": st.session_state['history']})
    answer, docs = res['result'], [] if False else res['source_documents']
    st.session_state['history'].append((query, answer))
    return answer

# Initialize chat history
if 'history' not in st.session_state:
    st.session_state['history'] = []

# Initialize messages
if 'generated' not in st.session_state:
    st.session_state['generated'] = ["Hello ! Ask me(mistral via ollama) about your doc(s) 🤗"]

if 'past' not in st.session_state:
    st.session_state['past'] = ["Hey ! 👋"]

# Create containers for chat history and user input
response_container = st.container()
container = st.container()

# User input form
with container:
    with st.form(key='my_form', clear_on_submit=True):
        user_input = st.text_input("Query:", placeholder="Talk to csv data 👉 (:", key='input')
        submit_button = st.form_submit_button(label='Send')

    if submit_button and user_input:
        output = conversational_chat(user_input)
        st.session_state['past'].append(user_input)
        st.session_state['generated'].append(output)

# Display chat history
if st.session_state['generated']:
    with response_container:
        for i in range(len(st.session_state['generated'])):
            message(st.session_state["past"][i], is_user=True, key=str(i) + '_user', avatar_style="big-smile")
            message(st.session_state["generated"][i], key=str(i), avatar_style="thumbs")
