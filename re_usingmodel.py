from langchain_community.llms import CTransformers
from langchain_community.embeddings import HuggingFaceEmbeddings
from langchain_community.vectorstores import FAISS
#from langchain import PromptTemplate
#refinofrom langchain.chains import RetrievalQA
from langchain.chains import ConversationalRetrievalChain
#from langchain_community.llms import Ollama
import time

# prepare the template we will use when prompting the AI
template = """Use the following pieces of information to answer the user's question.
If you don't know the answer, just say that you don't know, don't try to make up an answer.
Context: {context}
Question: {question}
Only return the helpful answer below and nothing else.
Helpful answer:
"""

# load the language model
#llm = CTransformers(model='./llama-2-7b-chat.ggmlv3.q4_0.bin',
#llm = CTransformers(model='../zephyr-7b-alpha.Q4_0.gguf',
llm = CTransformers(model='/mnt/c/Users/nouam/Projects/mistral-7b-v0.1.Q8_0.gguf',
                    model_type='llama',
                    config={'max_new_tokens': 256, 'temperature': 0.01})
#llm = Ollama(model="mistral:7b-instruct-v0.2-q8_0")

# load the interpreted information from the local database
embeddings = HuggingFaceEmbeddings(
    model_name="sentence-transformers/all-MiniLM-L6-v2",
    model_kwargs={'device': 'cpu'})
db = FAISS.load_local("faiss", embeddings)

# prepare a version of the llm pre-loaded with the local content
retriever = db.as_retriever(search_kwargs={'k': 3})
#prompt = PromptTemplate(
#    template=template,
#    input_variables=['context', 'question'])
#qa_llm = RetrievalQA.from_chain_type(llm=llm,
#                                     chain_type='stuff',
#                                     retriever=retriever,
#                                     return_source_documents=True,
#                                     chain_type_kwargs={'prompt': prompt})
qa= ConversationalRetrievalChain.from_llm(llm=llm, retriever=retriever, chain_type='stuff')

while True:
   query = input("\nEnter a query: ")
   if query == "exit":
      break
   if query.strip() == "":
      continue
   
   # Get the answer from the chain
   chat_history=[]
   start = time.time()
   result=qa({"question": query, "chat_history": chat_history})
   chat_history=[(query, result["answer"])]
   end = time.time()
   
   # Print the result
   print("\n\n> Question:")
   print(query)
   print(result["answer"])
   print(" time: {}".format(end-start))
