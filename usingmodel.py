from langchain.llms import CTransformers
from langchain.embeddings import HuggingFaceEmbeddings
from langchain.vectorstores import FAISS
from langchain import PromptTemplate
from langchain.chains import RetrievalQA
#from time import time

# prepare the template we will use when prompting the AI
template = """Use the following pieces of information to answer the user's question.
If you don't know the answer, just say that you don't know, don't try to make up an answer.
Context: {context}
Question: {question}
Only return the helpful answer below and nothing else.
Helpful answer:
"""

# load the language model
llm = CTransformers(model='./llama-2-7b-chat.ggmlv3.q4_0.bin',
                    model_type='llama',
                    config={'max_new_tokens': 256, 'temperature': 0.01})

# load the interpreted information from the local database
embeddings = HuggingFaceEmbeddings(
    model_name="sentence-transformers/all-MiniLM-L6-v2",
    model_kwargs={'device': 'cpu'})
db = FAISS.load_local("faiss", embeddings)

# prepare a version of the llm pre-loaded with the local content
retriever = db.as_retriever(search_kwargs={'k': 2})
prompt = PromptTemplate(
    template=template,
    input_variables=['context', 'question'])
qa_llm = RetrievalQA.from_chain_type(llm=llm,
                                     chain_type='stuff',
                                     retriever=retriever,
                                     return_source_documents=True,
                                     chain_type_kwargs={'prompt': prompt})
def query(model, question):
#    model_path = model.combine_documents_chain.llm_chain.llm.model
#    model_name = pathlib.Path(model_path).name
#    time_start = time.time()
    output = model({'query': question})
    response = output["result"]
#    time_elapsed = time.time() - time_start
#    display(HTML(f'<code>{model_name} response time: {time_elapsed:.02f} sec</code>'))
#    display(HTML(f'<strong>Question:</strong> {question}'))
#    display(HTML(f'<strong>Answer:</strong> {response}'))
    print("question: " + question)
    print("answer: " + response)

#query(qa_llm, "what is the intent of Zelenskyy?")

